#!/bin/bash
#
src=(
autostart.patch
ipc.patch
kblayout.patch
vanitygaps.patch
master-right.patch
quit-pipewire-fix.patch
restartdwl.patch
shiftview.patch
squibidmoveKeyboard.patch
switchtotag.patch
xcursor.patch
)

for i in ${src[@]}; do
	patch -p1 < ../git-branch-patches/$i || exit 1
done

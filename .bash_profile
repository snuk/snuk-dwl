# 
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

export PATH=$PATH:$HOME/bin:$HOME/.local/bin:$HOME/.cargo/bin

export QT_QPA_PLATFORM=wayland
export XDG_CURRENT_DESKTOP=dwl
export XDG_SESSION_DESKTOP=dwl
export XDG_CURRENT_SESSION_TYPE=wayland
export MOZ_ENABLE_WAYLAND=1
export GDK_BACKEND="wayland,x11" 

export WLR_NO_HARDWARE_CURSORS=1

export LANG=en_GB.UTF-8
export LC_COLLATE=C
export MESA_LOADER_DRIVER_OVERRIDE=i965

export BROWSER="firefox"
export XCURSOR_THEME="whiteglass"


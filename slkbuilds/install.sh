#!/bin/bash
#
src=(
#ninja
#meson
#pipewire
#libdrm
#wayland
#wayland-protocols
#xcb-util-errors
tllist
wlr-randr
uthash
scdoc
seatd
libliftoff
libdisplay-info
#pixman
wlroots
slurp
grim
wbg
lbonn-rofi-wayland
ttf-font-awesome
ttf-font-fork-awesome
fcft
libfdk-aac
x264
ffmpeg
wf-recorder
3270-nerd-ttf
wev
foot
)

for i in ${src[@]}; do
	cd $i || exit 1
	sh $i.SlackBuild || exit 1
	upgradepkg --install-new --reinstall /tmp/$i*_SBo.t?z || exit 1
	cd ..
done


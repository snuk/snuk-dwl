#!/bin/bash
#
src=(
ninja
meson
libxkbcommon
libglvnd
pipewire
libdrm
pango
gtk4
libinput
wayland-protocols
xcb-util-errors
tllist
wlr-randr
uthash
scdoc
seatd
libliftoff
libdisplay-info
pixman
wayland
wlroots
slurp
grim
wbg
lbonn-rofi-wayland
ttf-font-awesome
ttf-font-fork-awesome
fcft
libfdk-aac
x264
ffmpeg
wf-recorder
wev
foot
)

for i in ${src[@]}; do
	cd $i || exit 1
	sh $i.SlackBuild || exit 1
	upgradepkg --install-new --reinstall /tmp/$i*.t?z || exit 1
	cd ..
done


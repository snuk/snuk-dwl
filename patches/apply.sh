#!/bin/bash
#
src=(
autostart-0.7.patch
ipc-0.7.patch
kblayout-0.7.patch
vanitygaps-0.7.patch
master-right-0.7.patch
quit-pipewire-fix-0.7.patch
restartdwl-0.7.patch
shiftview-0.7.patch
squibidmoveKeyboard-0.7.patch
switchtotag-0.7.patch
xcursor-0.7.patch
)

for i in ${src[@]}; do
	patch -p1 < ../patches/$i || exit 1
done

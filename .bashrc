#.bashrc
# 16 March 2015
#
# Add bin to path
#
#
if [[ ${EUID} == 0 ]] ; then
#root-user
PS1="\[\e[34m\][\[\e[m\]\[\e[31m\]\u\[\e[m\]\[\e[31m\]@\[\e[m\]\[\e[31m\]\h\[\e[m\] \w \[\e[34m\]]\[\e[m\] "
   else
#user
PS1="\[\e[31m\][\[\e[m\]\[\e[33m\]\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[36m\]\h\[\e[m\] \w \[\e[31m\]]\[\e[m\] "
fi

# Add color
#
eval `dircolors -b`
#
# Check and resize the window as needed
#
#
# User defined aliases
#
alias ls='ls --color=auto'
